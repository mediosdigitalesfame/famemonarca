<!doctype html>

<html lang="es" xml:lang="es" class="no-js">

<head>

<?php include('seguimientos.php'); ?>

	<title>Contacto</title>

	<?php include('contenido/head.php'); ?>

</head>

<body>

<?php include('chat.php'); ?>

	<!-- Container -->

	<div id="container">

		<?php include('contenido/header.php'); ?>

		 <?php include('contenido/analytics.php'); ?>

		<div id="content">



			<!-- Page Banner -->

			<div class="page-banner">

				<div class="container">

					<h2>Contáctanos</h2>



				</div>

			</div>





			<!-- contact box -->

			<div class="contact-box">

				<div class="container">

					<div class="row">

                        

                        	<div class="col-md-6" align="center">

                                 <div class="container">

					                 <div class="col-md-12" >

								         <?php include('jotform.php'); ?>

                                     </div>

                                 </div>

                             </div>

						<div class="col-md-3">

							<div class="contact-information">

								<h3>Información de Contacto</h3>

								<ul class="contact-information-list">

									<li><span><i class="fa fa-home"></i>Av. Acueducto #2865</span> <span>Col. Lomas de Hidalgo. </span> <span> Morelia, Michoacán</span></li>

									<li><span><i class="fa fa-phone"></i>(443) 315 2244</span></li>

                                    <li><i class="fa fa-phone"></i><span>Taller de Servicio <strong>Ext. 124</strong></span><br>

									<i class="fa fa-phone"></i><span>Refacciones <strong>Ext. 119</strong></span><br>

                                    <i class="fa fa-phone"></i><span>Ventas <strong>Directo</strong></span><br>

                                    <i class="fa fa-phone"></i><span>Recepción <strong>Directo</strong></span><br>                                    

                                    <i class="fa fa-phone"></i><span>Seminuevos <strong>Ext. 115</strong></span><br>

                                    <i class="fa fa-phone"></i><span>Contabilidad <strong>Ext. 112</strong></span><br>

                                    <i class="fa fa-phone"></i><span>Financiamiento <strong>Ext. 113</strong></span><br>                             

                                    </li>

                                    <h3>Whatsapp</h3> 



    								<li><span><i class="fa fa-whatsapp"></i><strong>   Ventas   y   Postventa <br> 4433958586 | 4432275211</strong></span></li>



<li><span><i class="fa fa-whatsapp"></i><strong>   Refacciones   y   Citas <br> 4432732795 | 4431551131</strong></span></li>





									<li><a href="#"><i class="fa fa-envelope"></i>recepcion@fememonarca.com</a></li>

								</ul>

							</div>

						</div>



						<div class="col-md-3">

							<div class="contact-information">

								<h3>Horario de Atención</h3>

								<p>Con gusto esperamos tu llamada en nuestro <strong>Call Center</strong>, para cualquier duda, aclaración o sugerencia que quieras compartirnos en <strong>FAME Honda Monarca Morelia</strong>; te escuchamos y atendemos de manera personalizada. </p>

								<p class="work-time"><span>Lunes - Viernes</span> : 9:00 a.m. - 7:00 p.m.</p>

								<p class="work-time"><span>Sábado</span> : 9:00 a.m. - 2:00 p.m.</p>

							</div>

						</div>

						

					

 

					</div>

				</div>

			</div>



		</div> 



		<br><br><br><br><br><br><br>

        

		<?php include('contenido/footer.php'); ?>

     </div> 			

	

</body>

</html>