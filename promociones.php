<!doctype html>
<html lang="es" xml:lang="es" class="no-js">

    <head>

<?php include('seguimientos.php'); ?>

    	<title>Promociones</title>
    	<?php include('contenido/head.php'); ?>
    </head>

    <body>
        <?php include('chat.php'); ?>
    	<!-- Container -->
    	<div id="container">
            
    		<?php include('contenido/header.php'); ?>
            <?php include('contenido/analytics.php'); ?>

            <div class="page-banner">         
                <div class="container"> 
                    <h2><strong>Promociones</strong></h2> 
                </div>
            </div>

            <div class="single-project-page">
                <div class="container">
                    <div class="row">
                        <!-- PROMO 1 ================================================= -->
                    
                         <span class="single-project-content">
                              <a href="contacto.php"> <img alt="Promociones HONDA" src="promos/BRV.jpg"></a>
                         </span>   

                         <span class="single-project-content">
                         </span>                        
                     
                         <div class="col-md-12">
                             <h2>BR-V</h2>
                             <p align="justify"> 
                               Vigencia del 01 al 30 de Junio de 2019 o hasta agotar existencia lo que suceda primero. Bono de $25,000.00 aplica en la compra de Honda® BR-V® en versión UNIQ y PRIME modelo 2018. Crédito exclusivo con Honda Finance. Aplican restricciones. No incluye accesorios del vehículo. No aplica con otras promociones. Para mayor información acerca de requisitos y opciones de financiamiento visite su Distribuidor autorizado Honda® más cercano.
                             </p><br>
                         </div><br><br> 


                   <!-- PROMO 1 ================================================= -->     

                    <!-- PROMO 1 ================================================= -->
                    
                         <span class="single-project-content">
                              <a href="contacto.php"> <img alt="Promociones HONDA" src="promos/CRV19.jpeg"></a>
                         </span>   

                         <span class="single-project-content">
                         </span>                        
                     
                         <div class="col-md-12">
                             <h2>CR-V</h2>
                             <p align="justify"> 
                               Vigencia del 01 al 30 de Junio de 2019 o hasta agotar existencia lo que suceda primero. Mensualidades de $6,385 en la compra de Honda® CR-V® en versión EX modelo 2019, con un pago desde 30% (treinta) de enganche y plazos de 72 meses de $6,384.03, con un CAT de 26.79% sin IVA promedio informativo, que considera el costo del seguro de vida, seguro de daños y comisión por apertura de crédito. Crédito exclusivo con Honda Finance. Aplican restricciones. No incluye accesorios del vehículo. No aplica con otras promociones. Para mayor información acerca de requisitos y opciones de financiamiento visite su Distribuidor autorizado Honda® más cercano.
                             </p><br>
                         </div><br><br> 


                   <!-- PROMO 1 ================================================= -->     

                    <!-- PROMO 1 ================================================= -->
                    
                         <span class="single-project-content">
                              <a href="contacto.php"> <img alt="Promociones HONDA" src="promos/ACCORD.jpg"></a>
                         </span>   

                         <span class="single-project-content">
                         </span>                        
                     
                         <div class="col-md-12">
                             <h2>ACCORD</h2>
                             <p align="justify"> 
                              Vigencia del 01 al 30 de Junio de 2019 o hasta agotar existencia lo que suceda primero. Mensualidades de 6,711 en la compra de Honda® Accord® en versión EX modelo 2019, con un pago desde 30% (treinta) de enganche y plazos de 72 meses de $6,710.81, con un CAT de 28.27% sin IVA promedio informativo, que considera el costo del seguro de vida, seguro de daños y comisión por apertura de crédito. Crédito exclusivo con Honda Finance. Aplican restricciones. No incluye accesorios del vehículo. No aplica con otras promociones. Para mayor información acerca de requisitos y opciones de financiamiento visite su Distribuidor autorizado Honda® más cercano.
                             </p><br>
                         </div><br><br> 


                   <!-- PROMO 1 ================================================= -->     

                    <!-- PROMO 1 ================================================= -->
                    
                         <span class="single-project-content">
                              <a href="contacto.php"> <img alt="Promociones HONDA" src="promos/HRV.jpg"></a>
                         </span>   

                         <span class="single-project-content">
                         </span>                        
                     
                         <div class="col-md-12">
                             <h2>HR-V</h2>
                             <p align="justify"> 
                               Vigencia del 01 al 30 de Junio de 2019 o hasta agotar existencia lo que suceda primero. Mensualidades de $4,850 en la compra de Honda® HR-V® en versión UNIQ MT modelo 2019, con un pago desde 30% (treinta) de enganche y plazos de 72 meses de $4,849.76, con un CAT de 29.1% sin IVA promedio informativo, que considera el costo del seguro de vida, seguro de daños y comisión por apertura de crédito. Crédito exclusivo con Honda Finance. Aplican restricciones. No incluye accesorios del vehículo. No aplica con otras promociones. Para mayor información acerca de requisitos y opciones de financiamiento visite su Distribuidor autorizado Honda® más cercano.
                             </p><br>
                         </div><br><br> 


                   <!-- PROMO 1 ================================================= -->     

                    <!-- PROMO 1 ================================================= -->
                    
                         <span class="single-project-content">
                              <a href="contacto.php"> <img alt="Promociones HONDA" src="promos/CRV18.jpg"></a>
                         </span>   

                         <span class="single-project-content">
                         </span>                        
                     
                         <div class="col-md-12">
                             <h2>CR-V</h2>
                             <p align="justify"> 
                               Vigencia del 01 al 30 de Junio de 2019 o hasta agotar existencia lo que suceda primero. Bono de $15,000.00 aplica en la compra de Honda® CR-V® en versiones EX CVT, TURBO PLUS CVT, TOURING CVT modelo 2018. Crédito exclusivo con Honda Finance. Aplican restricciones. No incluye accesorios del vehículo. No aplica con otras promociones. Para mayor información acerca de requisitos y opciones de financiamiento visite su Distribuidor autorizado Honda® más cercano.
                             </p><br>
                         </div><br><br> 


                   <!-- PROMO 1 ================================================= -->     

                    <!-- PROMO 1 ================================================= -->
                    
                         <span class="single-project-content">
                              <a href="contacto.php"> <img alt="Promociones HONDA" src="promos/ACCORD18.jpg"></a>
                         </span>   

                         <span class="single-project-content">
                         </span>                        
                     
                         <div class="col-md-12">
                             <h2>ACCORD</h2>
                             <p align="justify"> 
                               Vigencia del 01 al 30 de Junio de 2019 o hasta agotar existencia lo que suceda primero. Bono de $15,000.00 aplica en la compra de Honda® Accord® en versión Ex CVT, Sport Plus y Touring modelo 2018, 0% de comisión por apertura en la compra de Honda® Accord® en versión Ex CVT y Sport Plus modelo 2018 con un pago desde 10% (diez) de enganche y plazos de 12 a 72, con un CAT de 29.28% sin IVA promedio informativo para modelo 2018, que considera el costo del seguro de vida, seguro de daños y comisión por apertura de crédito. Crédito exclusivo con Honda Finance. Aplican restricciones. No incluye accesorios del vehículo. No aplica con otras promociones. Para mayor información acerca de requisitos y opciones de financiamiento visite su Distribuidor autorizado Honda® más cercano.
                             </p><br>
                         </div><br><br> 


                   <!-- PROMO 1 ================================================= -->     

                    <!-- PROMO 1 ================================================= -->
                    
                         <span class="single-project-content">
                              <a href="contacto.php"> <img alt="Promociones HONDA" src="promos/PILOT.jpg"></a>
                         </span>   

                         <span class="single-project-content">
                         </span>                        
                     
                         <div class="col-md-12">
                             <h2>PILOT</h2>
                             <p align="justify"> 
                               Vigencia del 01 al 30 de Junio de 2019 o hasta agotar existencia lo que suceda primero. Bono de $20,000.00 aplica en la compra de Honda® Pilot® en versión PRIME modelo 2019. Crédito exclusivo con Honda Finance. Aplican restricciones. No incluye accesorios del vehículo. No aplica con otras promociones. Para mayor información acerca de requisitos y opciones de financiamiento visite su Distribuidor autorizado Honda® más cercano.
                             </p><br>
                         </div><br><br> 


                   <!-- PROMO 1 ================================================= -->     

                    <!-- PROMO 1 ================================================= -->
                    
                         <span class="single-project-content">
                              <a href="contacto.php"> <img alt="Promociones HONDA" src="promos/CIVIC.jpg"></a>
                         </span>   

                         <span class="single-project-content">
                         </span>                        
                     
                         <div class="col-md-12">
                             <h2>CIVIC</h2>
                             <p align="justify"> 
                               Vigencia del 01 al 30 de Junio de 2019 o hasta agotar existencia lo que suceda primero. Bono de $10,000.00 aplica en la compra de Honda® Civic® en versión EX, I-STYLE CVT, COUPE CVT, TURBO PLUS CVT y TOURING CVT en modelos 2018. Crédito exclusivo con Honda Finance. Aplican restricciones. No incluye accesorios del vehículo. No aplica con otras promociones. Para mayor información acerca de requisitos y opciones de financiamiento visite su Distribuidor autorizado Honda® más cercano.
                             </p><br>
                         </div><br><br> 


                   <!-- PROMO 1 ================================================= -->     

                    <!-- PROMO 1 ================================================= -->
                    
                         <span class="single-project-content">
                              <a href="contacto.php"> <img alt="Promociones HONDA" src="promos/TYPE-R.jpg"></a>
                         </span>   

                         <span class="single-project-content">
                         </span>                        
                     
                         <div class="col-md-12">
                             <h2>TYPE-R</h2>
                             <p align="justify"> 
                              Vigencia del 01 al 30 de Junio de 2019 o hasta agotar existencia lo que suceda primero. Tasas desde 11.99% en la compra de Honda® Type-R® modelo 2017 y 2018, con un pago desde 40% de enganche y plazos desde 12 a 72 meses, con un CAT de 33.96% sin IVA promedio informativo para modelo 2018 y CAT de 35.84% sin IVA promedio informativo para modelo 2017, que considera el costo del seguro de vida, seguro de daños y comisión por apertura de crédito. Crédito exclusivo con Honda Finance. Aplican restricciones. No incluye accesorios del vehículo. No aplica con otras promociones. Para mayor información acerca de requisitos y opciones de financiamiento visite su Distribuidor autorizado Honda® más cercano.
                             </p><br>
                         </div><br><br> 


                   <!-- PROMO 1 ================================================= -->     

                    <!-- PROMO 1 ================================================= -->
                    
                         <span class="single-project-content">
                              <a href="contacto.php"> <img alt="Promociones HONDA" src="promos/ODYSSEY.jpg"></a>
                         </span>   

                         <span class="single-project-content">
                         </span>                        
                     
                         <div class="col-md-12">
                             <h2>ODYSSEY</h2>
                             <p align="justify"> 
                               Vigencia del 01 al 30 de Junio de 2019 o hasta agotar existencia lo que suceda primero. Tasas desde 11.99% en la compra de Honda® Odyssey® en versión Prime o Touring modelo 2019, con un pago desde 40% de enganche y plazos desde 12 a 72 meses, con un CAT de 26.97% sin IVA promedio informativo, que considera el costo del seguro de vida, seguro de daños y comisión por apertura de crédito. Crédito exclusivo con Honda Finance. Aplican restricciones. No incluye accesorios del vehículo. No aplica con otras promociones. Para mayor información acerca de requisitos y opciones de financiamiento visite su Distribuidor autorizado Honda® más cercano.
                             </p><br>
                         </div><br><br> 


                   <!-- PROMO 1 ================================================= -->     


                    </div>
                </div>
            </div>                      
            <?php include('contenido/footer.php'); ?>
        </div>
    </body>
</html>