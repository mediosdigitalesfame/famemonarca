<?php
if($_POST)
{
	require('constant.php');

	$user_name      = filter_var($_POST["name"], FILTER_SANITIZE_STRING);
	$user_email     = filter_var($_POST["email"], FILTER_SANITIZE_EMAIL);
	$user_phone     = filter_var($_POST["phone"], FILTER_SANITIZE_STRING);
	$user_modelo     = filter_var($_POST["modelo"], FILTER_SANITIZE_STRING);
	$user_servicio     = filter_var($_POST["servicio"], FILTER_SANITIZE_STRING);
	$content   = filter_var($_POST["content"], FILTER_SANITIZE_STRING);

	if(empty($user_name)) {
		$empty[] = "<b>Nombre</b>";		
	}
	if(empty($user_email)) {
		$empty[] = "<b>Email</b>";
	}
	if(empty($user_phone)) {
		$empty[] = "<b>Telefono</b>";
	}	
	if(empty($user_modelo)) {
		$empty[] = "<b>Modelo</b>";
	}	
	if(empty($user_servicio)) {
		$empty[] = "<b>Servicio</b>";
	}	
	if(empty($content)) {
		$empty[] = "<b>Comentario</b>";
	}
	
	if(!empty($empty)) {
		$output = json_encode(array('type'=>'error', 'text' => implode(", ",$empty) . ' REQUERIDO!'));
		die($output);
	}
	
	if(!filter_var($user_email, FILTER_VALIDATE_EMAIL)){ //email validation
		$output = json_encode(array('type'=>'error', 'text' => '<b>'.$user_email.'</b> El correo es invalido, ingresa uno correcto por favor.'));
		die($output);
	}
	
	//reCAPTCHA validation
	if (isset($_POST['g-recaptcha-response'])) {
		
		require('component/recaptcha/src/autoload.php');		
		
		$recaptcha = new \ReCaptcha\ReCaptcha(SECRET_KEY, new \ReCaptcha\RequestMethod\SocketPost());

		$resp = $recaptcha->verify($_POST['g-recaptcha-response'], $_SERVER['REMOTE_ADDR']);

		if (!$resp->isSuccess()) {
			$output = json_encode(array('type'=>'error', 'text' => '<b>Captcha</b> Verificacion requerida!'));
			die($output);				
		}	
	}


	if($user_servicio == 'Cotizacion') 
	{
		$toEmail = "asesoronline2@contactcenterfame.com". ',' . "mediosdigitales@grupofame.com";
		$servicio = "Cotizacion de Vehiculo";
	}	 
	else if($user_servicio == 'Manejo')
	{
		$toEmail = "asesoronline2@contactcenterfame.com". ',' . "mediosdigitales@grupofame.com" ;
		$servicio = "Prueba de Manejo";
	}	
	else if($user_servicio == 'Servicio')
	{
		$toEmail = "servicio@famemonarca.com" . ','. "formas@grupofame.com" . ',' . "gerencia@famemonarca.com" . ',' . "mediosdigitales@grupofame.com";
		$servicio = "Cita de Servicio";
	}
	else	
	{
		$toEmail = "ventas@famemonarca.com, gerencia@famemonarca.com" . ','. "formas@grupofame.com" . ',' . "mediosdigitales@grupofame.com";
		$servicio = "Cotizacion de Refacciones";
	}
	

	$mailHeaders = "From: " . $servicio . "<" . $user_name . ">\r\n";
	$mailBody = "Nombre: " . $user_name . "\n";
	$mailBody .= "Email: " . $user_email . "\n";
	$mailBody .= "Telefono: " . $user_phone . "\n";
	$mailBody .= "Modelo: " . $user_modelo . "\n";
	$mailBody .= "Servicio: " . $user_servicio . "\n";
	$mailBody .= "Comentario: " . $content . "\n";

	if (mail($toEmail, "Formulario de Contato Honda Monarca - Grupo FAME", $mailBody, $mailHeaders)) {

		$output = json_encode(array('type'=>'message', 'text' => 'Hola '.$user_name .', Gracias por tus datos. En breve uno de nuestros asesores se pondra en contacto.'));
		die($output);


	} else {
		$output = json_encode(array('type'=>'error', 'text' => 'No se puede enviar, intenta de nuevo mas tarde.'.SENDER_EMAIL));
		die($output);
	}
}

?>