<?php
require('constant.php');
?>
<!doctype html>
<html lang="es" class="no-js">
<head>
	<title>Grupo FAME - Contacto</title>
	<script src="component/jquery/jquery-3.2.1.min.js"></script>
	<script>

		$(document).ready(function (e){
			$("#frmContact").on('submit',(function(e){
				e.preventDefault();
				$("#mail-status").hide();
				$('#send-message').hide();
				$('#loader-icon').show();
				$.ajax({
					url: "contact.php",
					type: "POST",
					dataType:'json',
					data: {
						"name":$('input[name="name"]').val(),
						"email":$('input[name="email"]').val(),
						"phone":$('input[name="phone"]').val(),
						"modelo":$('select[name="modelo"]').val(),
						"servicio":$('select[name="servicio"]').val(),
						"content":$('textarea[name="content"]').val(),
						"g-recaptcha-response":$('textarea[id="g-recaptcha-response"]').val()},				
						success: function(response){
							$("#mail-status").show();
							$('#loader-icon').hide();
							if(response.type == "error") {
								$('#send-message').show();
								$("#mail-status").attr("class","error");				
							} else if(response.type == "message"){
								$('#send-message').hide();
								$("#mail-status").attr("class","success");	
							}
							$("#mail-status").html(response.text);	
							document.getElementById("#frmContact").reset();
						},
						error: function(){} 
					});
			}));
		});
	</script>

	<script src='https://www.google.com/recaptcha/api.js'></script>	
	<?php include('contenido/head.php'); ?>

	<style>
	#message {  padding: 0px 40px 0px 0px; }
	#mail-status {
		padding: 12px 20px;
		width: 100%;
		display:none; 
		font-size: 1em;
		font-family: "Georgia", Times, serif;
		color: rgb(40, 40, 40);
	}
	.error{background-color: #F7902D;  margin-bottom: 40px;}
	.success{background-color: #48e0a4; }
	.g-recaptcha {margin: 0 0 25px 0;}	  
</style>

</head> 

<body>

	<div id="container">
		<?php include('../contenido/headersub.php'); ?>   
		<div class="page-banner">         
			<div class="container">
				<h2>Contacto</h2>
			</div>
		</div>

		<div class="contact-box">
			<div class="container">

				<div class="row">

					<div class="col-md-6" align="center">
						<?php include('formulario.php'); ?>
					</div>

					<div class="col-md-3">
						<div class="contact-information">
							<h3>Información de Contacto</h3>
							<ul class="contact-information-list">
								<li><span><i class="fa fa-phone"></i>01800 670 8386 <strong>Call Center</strong></span></li>
								<li><a href="#"><i class="fa fa-envelope"></i>contacto@grupofame.com</a></li>
							</ul>
						</div>
					</div>

					<div class="col-md-3">
						<div class="contact-information">
							<h3>Horario de Atención</h3>
							<p>Con gusto esperamos tu llamada en nuestro <strong>Call Center</strong>, para cualquier duda, aclaración o sugerencia que quieras compartirnos en Grupo FAME te escuchamos y atendemos de manera personalizada. </p>
							<p class="work-time"><span>Lunes - Viernes</span> : 9:00 AM a 2:00 PM - 4:00 AM a 7:00 PM</p>
							<p class="work-time"><span>Sábado</span> : 9:00 AM - 1:00 PM.</p>
							<p class="work-time"><span>Domingo</span> <i>Cerrado</i></p>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</body>

<?php include_once('../contenido/footer.php'); ?>






