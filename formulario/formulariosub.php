<div class="container">
							<div class="col-md-12" >

								<div class="container">
									<div id="message">
										<form class="well2 form-horizontal" id="frmContact" action="" method="POST" novalidate="novalidate">

											<fieldset>

												<h2><legend2>¡Nosotros te Contactamos!</legend2></h2>
												<labelform>
													<!-- Input NOMBRE-->
													<div class="form-group">  
														<div class="col-md-12 inputGroupContainer">
															<div class="input-group">
																<span class="input-group-addon2"><i class="fa fa-user"></i></span>
																<input class="form-control" type="text" id="name" name="name" placeholder="Escribe tu nombre aqui" title="Please enter your name" class="required" aria-required="true" required>
															</div>
														</div>
													</div>

													<!-- Input MAIL-->
													<div class="form-group"> 
														<div class="col-md-12 inputGroupContainer">
															<div class="input-group">
																<span class="input-group-addon2"><i class="fa fa-envelope"></i></span>
																<input class="form-control" type="text" id="email" name="email" placeholder="Escribe tu email aqui" title="Please enter your email address" class="required email" aria-required="true" required>
															</div>
														</div>
													</div>

													<!-- Input TELEFONO-->
													<div class="form-group">  
														<div class="col-md-12 inputGroupContainer">
															<div class="input-group">
																<span class="input-group-addon2"><i class="fa fa-phone"></i></span>
																<input class="form-control" type="text" id="phone" name="phone" placeholder="Escribe tu telefono aqui" title="Please enter your phone number" class="required phone" aria-required="true" required>
															</div>
														</div>
													</div>

													<!-- Select MODELO -->
													<div class="form-group">
														<div class="col-md-12 selectContainer">
															<div class="input-group">
																<span class="input-group-addon2"><i class="fa fa-cog"></i></span>
																<select name="modelo" id="modelo" class="form-control selectpicker" class="required" aria-required="true" required>
																	<option value='0' >Selecciona un Modelo</option>
																	<option name="m1" id="Type R">Type R</option>
																	<option name="m2" id="BR-v">BR-V</option>
																	<option name="m3" id="CR-V">CR-V</option>
																	<option name="m4" id="Civic Coupe">CIVIC Coupe</option>
																	<option name="m5" id="Civic">CIVIC</option>
																	<option name="m6" id="HR-V">HR-V</option>
																	<option name="m7" id="FIT">FIT</option>
																	<option name="m8" id="CITY">CITY</option>
																	<option name="m9" id="ACCORD">ACCORD</option>
																	<option name="m10" id="ODYSSEY">ODYSSEY</option>
																	<option name="m11" id="PILOT">PILOT</option>
																</select>
															</div>
														</div>
													</div>

													<!-- Select SERVICIO -->
													<div class="form-group">
														<div class="col-md-12 selectContainer">
															<div class="input-group">
																<span class="input-group-addon2"><i class="fa fa-cog"></i></span>
																<select name="servicio" id="servicio" class="form-control selectpicker" class="required" aria-required="true" required>
																	<option value='0' >Selecciona una Opción</option>
																	<option value='Cotizacion' name="s1" id="s1">Cotización de Vehículo</option>
																	<option value='Manejo' name="s2" id="s2">Prueba de Manejo GRATIS</option>
																	<option value='Servicio' name="s3" id="s3">Cita de Servicio</option>
																	<option value='Refacciones' name="s4" id="s4">Cotizar Refacciones</option>
																</select>
															</div>
														</div>
													</div>

													<!-- Text area --> 
													<div class="form-group">
														<div class="col-md-12 inputGroupContainer">
															<div class="input-group">
																<span class="input-group-addon2"><i class="fa fa-pencil"></i></span>
																<textarea class="form-control" id="comment-content" name="content" placeholder="Dejanos un comentario aqui"></textarea>
															</div>
														</div>
													</div>

													<div class="g-recaptcha" data-sitekey="<?php echo SITE_KEY; ?>"></div>			
													<div id="mail-status"></div>
													<br>
													<button type="Submit" id="send-message" class="btn btn-succes" >ENVIAR <span class="fa fa-paper-plane"></span></button>

												</labelform>







											</fieldset>  

										</form>

										<div id="loader-icon" style="display:none;"><img src="../formulario/img/loader.gif" /></div>

									</div>
								</div><!-- /.container -->

							</div>
						</div>