<!doctype html>

<html lang="es" xml:lang="es" class="no-js">

<head>

<?php include('seguimientos.php'); ?>

	<title>Honda Finance</title>

	<?php include('contenido/head.php'); ?>

</head>

<body>



	<?php include('chat.php'); ?>



	<!-- Container -->

	<div id="container">

		<?php include('contenido/header.php'); ?>

         <?php include('contenido/analytics.php'); ?>

		<div id="content">



			<div class="page-banner">         



				<div class="container">

					<h2>HONDA FINANCE</h2>

				</div>

			</div>



			<div class="about-box">

				<div class="container">

					<div class="row">

				</div>

			</div>







<!--barra información limpia-->

		<div class="section">

			<div id="about-section">



				<div class="welcome-box">

					<div class="container">

<!--						<h1><span>Nosotros</span></h1><br>-->

						<p align="justify">Es un plan de financiamiento disponible en el mercado a nivel nacional para la adquisición de autos nuevos o seminuevos HONDA®. Con HONDA FINANCE podrás acceder a un crédito con garantía prendaria, para la adquisición de cualquier auto.</p><br><br>

                        

<p align="left"><strong>HONDA FINANCE te ofrece grandes ventajas:</strong></p><br>                        



<p align="justify">

- Enganche desde 10%<br>

- Plazos de 6 a 72 meses<br>

- Tasa fija y pagos fijos en moneda nacional durante toda la vida del crédito (IVA incluido)</p><br><br>



<p> Alcanza tus sueños más veloces con... <strong>Honda Finance Service</strong></p><br><br>





<p align="left"><strong>Requisitos</strong></p><br>  

<p align="justify">En <strong>Honda Finance</strong> es muy fácil tramitar tu crédito, únicamente necesitas entregar en la concesionaria de tu preferencia los siguientes documentos:<br><br>



- Solicitud de crédito Honda finance <br>

- Comprobante de domicilio<br>

- Identificación oficial vigente con fotografía<br>

- Tener entre 18 y 70 años</p><br><br>



<p align="left"><strong>Formas de pago</strong></p><br>

<p align="justify">Las mensualidades de tu financiamiento serán cubiertas de acuerdo a la opción que elijas.</p><br><br>

 

<p align="left"><strong>Cargo a cuenta de cheques</strong></p><br>

<p align="justify">Contar con fondos suficientes para cubrir la mensualidad por lo menos un día antes de la fecha que aparecerá en tu estado de cuenta.</p><br><br>



<p align="left"><strong>Pago en quincenas</strong></p><br>

<p align="justify">Al poder negociar con la concesionaria automotriz el mejor precio de contado.</p><br><br>



<p align="left"><strong>Domicialización Interbancaria</strong></p><br>

<p align="justify">Podrás solicitar que el pago de tu crédito Honda Finance sea cargado a la cuenta de cheques del banco de tu preferencia, si no tienes ninguna cuenta para domiciliar el pago en otra institución, podrás obtener tu cuenta de cheques Bancomer.</p><br><br>



                	</div>

				</div>

 

	<?php include('contenido/footer.php'); ?>	 



</body>

</html>